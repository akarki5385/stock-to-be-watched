FROM golang

ENV G0111MODULE=on

ADD . /go/src/stock-to-be-watched-1

WORKDIR /go/src/stock-to-be-watched-1

COPY . .

RUN go install main.go

ENTRYPOINT  /go/bin/main --host 0.0.0.0


EXPOSE 8080
