package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"

	"github.com/gorilla/mux"
)

var inmemory = make(map[string]Stock)

//stock will hold all the stock information
type Stock struct {
	Name   string  `json:"name"`
	Symbol string  `json:"symbol"`
	Price  float64 `json:"price"`
	Low    float64 `json:"low"`
	High   float64 `json:"high"`
}

func main() {

	router := mux.NewRouter()

	router.HandleFunc("/stock", createStockHandler).Methods(http.MethodPost)
	router.HandleFunc("/stock", listStockHandler).Methods(http.MethodGet)

	router.HandleFunc("/stock/{id}", updateStockHandler).Methods(http.MethodPatch)
	router.HandleFunc("/stock/{id}", deleteStockHandler).Methods(http.MethodDelete)
	router.HandleFunc("/stock/{id}", viewStockHandler).Methods(http.MethodGet)

	fmt.Println("it should print something")
	http.ListenAndServe(":8080", router)
}

func createStockHandler(resp http.ResponseWriter, req *http.Request) {
	//decoding the incoming request

	body, err := ioutil.ReadAll(req.Body)

	//the following is the way to handle the error
	if err != nil {
		resp.WriteHeader(http.StatusBadRequest)
		return
	}

	var a Stock
	//convert incoming json request to golang

	err = json.Unmarshal(body, &a)

	if err != nil {
		resp.WriteHeader(http.StatusBadRequest)
		return
	}
	//make validaton

	if a.Symbol == "" || a.Name == "" || a.Price == 0 {
		resp.WriteHeader(http.StatusBadRequest)
		return
	}

	//store information in db
	inmemory[a.Symbol] = a
	resp.WriteHeader(http.StatusCreated)
}

func updateStockHandler(resp http.ResponseWriter, req *http.Request) {

	//read id from URI
	vars := mux.Vars(req)
	stockID := vars["id"]

	//check if the id is valid or not

	stock, ok := inmemory[stockID]
	if !ok {
		resp.WriteHeader(http.StatusNotFound)
		resp.Write([]byte("Given ID not found in our Database"))
		return
	}
	//decoding the incoming request
	body, err := ioutil.ReadAll(req.Body)
	//the following is the way to handle the error
	if err != nil {
		resp.WriteHeader(http.StatusBadRequest)
		return
	}

	var change Stock

	//convert incoming json request to golang
	err = json.Unmarshal(body, &change)

	if err != nil {
		resp.WriteHeader(http.StatusBadRequest)
		return
	}
	if change.Name != "" {
		stock.Name = change.Name
	}

	if change.Price != 0 {
		stock.Price = change.Price
	}

	if change.High != 0 {
		stock.High = change.High
	}

	if change.Low != 0 {
		stock.Low = change.Low
	}

	inmemory[stockID] = stock
	//return sucess response.
	resp.WriteHeader(http.StatusNoContent)

}

func deleteStockHandler(resp http.ResponseWriter, req *http.Request) {

	//read id from URI
	vars := mux.Vars(req)

	stockID := vars["id"]

	//check if the id is valid or not

	_, ok := inmemory[stockID]
	if !ok {
		resp.WriteHeader(http.StatusNotFound)
		return
	}
	// if valid, delete stock details of that ID
	delete(inmemory, stockID)

	//return sucess response.
	resp.WriteHeader(http.StatusNoContent)
}

func viewStockHandler(resp http.ResponseWriter, req *http.Request) {

	//read id from URI
	vars := mux.Vars(req)
	stockID := vars["id"]

	//check if the id is valid or not

	stock, ok := inmemory[stockID]
	if !ok {
		resp.WriteHeader(http.StatusNotFound)
		return
	}

	jsonStock, err := json.Marshal(stock)
	if err != nil {
		resp.WriteHeader(http.StatusInternalServerError)
		return
	}

	//return sucess response.
	resp.WriteHeader(http.StatusOK)
	resp.Write(jsonStock)

}

func listStockHandler(resp http.ResponseWriter, req *http.Request) {

	//convert go map to JSON
	convertedData, err := json.Marshal(inmemory)
	if err != nil {
		resp.WriteHeader(http.StatusInternalServerError)

		return
	}

	//send it in response it in status code

	resp.WriteHeader(http.StatusOK)
	resp.Write(convertedData)

}
